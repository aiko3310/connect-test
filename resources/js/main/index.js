/* eslint-disable no-extend-native */
/* global Vue axios */
/* eslint no-undef: "error" */
var dialog = new Vue({
  el: '#main',
  data: {
    loginData: {
      isLogin: false,
      data: {
        userName: '', 
        account: '', 
        role: ''
      }
    },
    logData: [// listDialog
      { Timestamp: '2019/1/9 上午 10:59:59', description: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' },
      { Timestamp: '2019/1/9 上午 10:59:59', description: 'BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB' },
      { Timestamp: '2019/1/9 上午 10:59:59', description: 'cccccccccccccccccccdfqwerwewfewfweqewefeqwfewfweqfwe' },
      {
        Timestamp: '2019/1/9 上午 10:59:59',
        description: '**********************************************************************************************************************'
      },
      { Timestamp: '2019/1/9 上午 10:59:59', description: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' }
    ],
    machItems: [// contentTop--machine connetion
    ],
    machHtml: $('.machtable'),
    deviceItems: [
      {
        device_name: '',
        device_ip: '',
        device_Mask: '',
        device_descr: '',
        device_info: '',
        connectType: ''
      }
    ],
    logDialogs: [
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx AAAAAA mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/15', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/18', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/19', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/20', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/21', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/22', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/23', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/24', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2018/5/25', logoDes: 'Txxx Cooooooooooooo mobile phone connection' },
      { timestamp: '2019/6/20', logoDes: 'bbbb dddddddddddooo mobile web connection' }
    ],
    keyword: '',
    checkNum: 0
  },
  created: function () {
    this.checkLogin()
    this.getAllMachines()
  },
  updated: function () {
    console.log('view updated');
    // if(lan == 'en'){
    $('.name .mobile-th').text('Name');
    $('.description .mobile-th').text('Description');
    $('#mach_connBy .mobile-th').text('User Connective');
    $('#mach_country .mobile-th').text('Country');
    $('#mach_city .mobile-th').text('City');
    $('#mach_remark .mobile-th').text('Remark');
    // }
  },
  mounted: function () {
    $('.header_menu ul li').click(function () { // mobileMenu hidden
      $('#burger').prop('checked', false);
    });

    $('.header_menu select').on('change', function () {
      $('#burger').prop('checked', false);
    });

    String.prototype.replaceAll = function (search, replacement) {
      var target = this;
      return target.replace(new RegExp(search, 'g'), replacement);
    };

    $('#show_search_btn').click(function () {
      $('.search').addClass('active');
    });

    $('#searchkeyword_btn').click(function () {
      var keyword;
      var hightlightContext;
      var context = $('.machtable tbody').html();
      var contextClear = context.replaceAll('<span class="hightLight">', '');
      $('.machtable tbody').html(contextClear);
      keyword = $('.search input').val();
      if (keyword !== '') {
        hightlightContext = contextClear.replaceAll(keyword, '<span class="hightLight">' + keyword + '</span>');
        $('.machtable tbody').html(hightlightContext);
      }

      // 因為search會使checkbox原有的勾選消失，因此以帶有".selected"屬性的資料列重新使其勾選狀態“
      $('.js-user-tr.selected input[type="checkbox"]').prop('checked', true);
    });

    $('#logDialog .close').click(function () {
      $('#logDialog').css('display', 'none');
    });

    $('.online .status').text('Online');
    $('.connecting .status').text('Connecting');
    $('.offline .status').text('Offline');

    $('.th').html('<th></th>\n'
            + '                <th>Status</th>\n'
            + '                <th>Name</th>\n'
            + '                <th>Description</th>\n'
            + '                <th>User Connective</th>\n'
            + '                <th>Country</th>\n'
            + '                <th>City</th>'
            + '                <th>Remark</th>');
    // machine --- en --start
    $('.name .mobile-th').text('Name');
    $('.description .mobile-th').text('Description');
    $('#mach_connBy .mobile-th').text('User Connective');
    $('#mach_country .mobile-th').text('Country');
    $('#mach_city .mobile-th').text('City');
    $('#mach_remark .mobile-th').text('Remark');
    // machine --- en --end

    $('.version .labelName').text('Version');

    $('#func_logout span').text('Logout');
    $('#func_mach_add span').text('Add');
    $('.js-btn-log span').text('Log');
    $('#func_map span').text('Map');
    $('#func_logout span').text('Logout');

    $('.sureBtn').text('Sure');
    $('.cancelBtn').text('Cancel');
  },
  methods: {
    form_cancel: function () {
      $('.form_buleBg input').val('');
      $('.translucentBg').css('display', 'none');
    },
    delete_cancel: function () {
      $('#qa_delete').css('display', 'none');
    },
    checkLogin: async function (){
      await fetch('http://211.75.8.115:18085/api/v1/account_info/',{
        method: 'get',
        mode: 'cors',
        credentials: 'include'
      }).then(res =>{
        if(res.status!==200){
          // document.location.pathname='http://211.75.8.115:18085/login';
          throw new Error(res.status)
        }else{
          return res.json()
        }
      }).then(data => {
        this.loginData.isLogin = true;
        this.loginData.data = {
          userName: data.acc_name,
          account: data.acc_mail,
          role: data.acc_identity
        }
      }).catch(function (error) {
        // document.location.pathname='http://211.75.8.115:18085/login';
        console.log(error);
      });
    },
    getAllMachines: async function(){
      var that = this
      await fetch('http://211.75.8.115:18085/api/v1/machines/',{
        method: 'get',
        mode: 'cors',
        credentials: 'include'
      }).then(res =>{
        if(res.status!==200){
          throw new Error(res.status)
        }else{
          return res.json()
        }
      }).then(data => {
        if(data){
          console.log('getAllMachines',data)
          that.machItems = Object.values(data)
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    logDialogShow: function () {
      $('#logDialog').toggle();
      // var image = document.getElementById('logDialog');
      // $('#logDialog').style.display = (image.style.display === 'none') ? 'block' : 'none';
    },

    logBtnClick: function (listName) {
      var logHtml = '';
      var i;

      for (i = 0; i < dialog.logData.length; i += 1) {
        logHtml += '<li><span class="timestamp">' + dialog.logData[i].Timestamp + '</span><span>' + dialog.logData[i].description + '</span></li>';
      }

      $('.listDialog .pageTile').text(listName);
      $('.listDialog .lists').html(logHtml);
      $('.contentTop').css('display', 'none');
      $('.contentDown').css('display', 'none');
      $('.listDialog').css('display', 'block');
      $('#logDialog').css('display', 'none');
    },
    // last page
    backToConnect: function () {
      $('.contentTop').css('display', 'block');
      $('.contentDown').css('display', 'block');
      $('.listDialog').css('display', 'none');
    },

    fillers: function () {
      $('#fillersInterface').css('display', 'block');
    },

    checkAll: function (obj, cName) {
      var checkboxs;
      var i;
      $('#loadingPage').css('visibility', 'visible');
      checkboxs = document.getElementsByName(cName);
      if (checkboxs.length > 0) {
        setTimeout(function () {
          $('#loadingPage').css('visibility', 'hidden');
          for (i = 0; i < checkboxs.length; i += 1) {
            checkboxs[i].checked = obj.checked;
            // console.log(obj.checked);
            if (obj.checked === true) {
              $(checkboxs[i]).closest('tr').addClass('selected');
              $('.main_control').addClass('active');
              $('.mobile_control').addClass('active');
            } else {
              $(checkboxs[i]).closest('tr').removeClass('selected');
              $('.main_control').removeClass('active');
              $('.mobile_control').removeClass('active');
            }
          }
        }, 1000);
      }
    },

    check: function (ff) {
      var f = ff;
      var checkedNums = 0;
      var i;
      var e;
      for (i = 0; i < f.length; i += 1) {
        e = f[i];
        if (e.type === 'checkbox' && e.checked) {
          checkedNums += 1;
        }
      }
      return checkedNums;
    },

    /* 只能選擇一筆，無法多選 */
    // /* global connectItemClick:true */
    connectItemClick: function (thisItem) {
      var item = thisItem.currentTarget;// 抓整組
      var theItem = $(item).find('input[type="checkbox"]');
      dialog.checkNum = $(item).attr('num');

      $('#loadingPage').css('visibility', 'visible');
      setTimeout(function () {
        if (theItem.prop('checked') === true) {
          theItem.prop('checked', false);
          $(item).removeClass('selected');
          if (dialog.check($('.tb__user table input[type="checkbox"]')) === 0) {
            $('.main_control').removeClass('active');
            $('.mobile_control').removeClass('active');
            console.log("control can't click");
          }
          console.log('remove checked!');
        } else {
          $('.tb__user table input[type="checkbox"]').prop('checked', false);
          $('.tb__user table .js-user-tr').removeClass('selected');

          $('.main_control').addClass('active');
          $('.mobile_control').addClass('active');
          theItem.prop('checked', true);
          $(item).addClass('selected');
          console.log('checked!!!');
        }
        $('#loadingPage').css('visibility', 'hidden');
      }, 1000);
    },

    /* 下面Active connection的裝置選取只能選擇一筆，無法多選 */
    deviceClick: function (thisDevice) {
      var device = thisDevice.currentTarget;// 抓整組
      var thedevice = $(device).find('input[type="checkbox"]');
      // console.log(device);

      $('#loadingPage').css('visibility', 'visible');
      setTimeout(function () {
        if (thedevice.prop('checked') === true) {
          thedevice.prop('checked', false);
          if (dialog.check($('.contentDown table input[type="checkbox"]')) === 0) {
            $('.contentDown .head_btn').removeClass('active');
          }
          console.log('remove device checked!');
        } else {
          $('.contentDown table input[type="checkbox"]').prop('checked', false);
          $('.contentDown .head_btn').addClass('active');
          thedevice.prop('checked', true);
          $(device).addClass('selected');
          console.log('device checked!!!');
        }
        $('#loadingPage').css('visibility', 'hidden');
      }, 600);
    },

    qa_deleteConnet: function () {
      $('#qa_delete').css('display', 'block');
    },
    deleteConnet: function (checkNum) {
      console.log($('.js-user-tr.selected'));
      $('#qa_delete').css('display', 'none');
      this.machItems.splice(checkNum, 1);
    },

    connectTheSelected: function () {
      var machConby = $('.js-user-tr.selected').find('#mach_connBy div:nth-child(2)').text();
      var machName = $('.js-user-tr.selected').find('#mach_name div:nth-child(2)').text();
      var linkUsersHtml = '<span class="linkUser"><span>' + machName + '</span><span>&lt;' + machConby + '&gt;</span></span>';
      $('.link-users').html(linkUsersHtml);
      $('.contentDown').css('display', 'block');
    },

    disConnect: function () {
      $('.contentDown').css('display', 'none');
    },

    filltering: function () {
      // render again
      $('#fillersInterface').css('display', 'none');
    },

    show_addNewConnect: function () {
      $('#addNewConnet').find('input').val('');
      $('.formName span').text('New My Connect');
      $('#addNewConnectBtn').css('display', 'block');
      $('#editConnectBtn').css('display', 'none');
      // $('#addNewConnet input#name').removeAttr('disabled');
      $('#addNewConnet').css('display', 'block');
    },
    addNewConnet: async function () {
      var that = this;
      var goalForm = $('#addNewConnet');
      var machName = goalForm.find('input[name="name"]').val();
      var machMacAddress = goalForm.find('input[name="mac_address"]').val();      
      var machConnectType = goalForm.find('input[name="connect_type"]').val();
      var machLngLat = goalForm.find('input[name="LngLat"]').val();
      var machDescription = goalForm.find('input[name="Description"]').val();
      var machCountry= goalForm.find('input[name="country"]').val();
      var machCity = goalForm.find('input[name="city"]').val();
      var machSn = goalForm.find('input[name="serialNumber"]').val();
      var machRemark = goalForm.find('input[name="remark"]').val();
      if (machName === '' || machName == null) {
        alert('Name can be null!');
      } else {
        // function checkIdrepeat(id){
        //   if(that.machItems.length >0){
        //     if(that.machItems.find(function(data){
        //       return data.mach_id === id
        //     }) > -1){
        //       return checkIdrepeat(Math.random().toString(36).substring(4))
        //     }else{
        //       return id
        //     }
        //   }else{
        //     return id
        //   }
        // }
        var dataObj = {
          mach_name: 'machName',
          mach_mac_address: 'machMacAddress',
          mach_connect_type: '02',
          mach_LngLat: '0,0',
          mach_descr: 'machDescription',
          mach_country: 'taiwan',
          mach_city: 'teipei',
          mach_sn: 'machSn',
          mach_remark:'machRemark'
          // mach_name: machName,
          // mach_mac_address: machMacAddress,
          // mach_connect_type: machConnectType,
          // mach_LngLat: machLngLat,
          // mach_descr: machDescription,
          // mach_country: machCountry,
          // mach_city: machCity,
          // mach_sn: machSn,
          // mach_remark:machRemark
        }
        await fetch('http://211.75.8.115:18085/api/v1/machines/',{
        method: 'post',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        body: JSON.stringify(dataObj)
      }).then(res =>{
        if(res.status!==200){
          throw new Error(res.status)
        }else{
          return res.json()
        }
      }).then(data => {
        if(data){
          console.log('addMachines',data)
        }
      }).catch(function (error) {
        console.log(error);
      });
      // 'Content-Type': 'text/plain'
                    // that.machItems.push({
        //   mach_status: '離線',
        //   mach_status_en: 'offline',
        //   mach_name: machName,
        //   mach_connBy: '',
        //   mach_country: machCountry,
        //   mach_city: machCity,
        //   mach_lanip: '',
        //   mach_descr: machDescr,
        //   mach_remark: machRemark
        // });
        // $('#addNewConnet').css('display', 'none');
      }
    },

    show_modifyConnect: function () {
      $('#addNewConnet').find('input').val('');
      $('#addNewConnet .formName span').text('Edit My Connect');
      $('#addNewConnectBtn').css('display', 'none');
      $('#editConnectBtn').css('display', 'block');
      $('#addNewConnet').css('display', 'block');
    },
    modifyConnect: function (num) {
      var obj = this.machItems;
      var goalForm = $('#addNewConnet');
      var machName = goalForm.find('input[name="name"]').val();
      var machCountry = goalForm.find('input[name="country"]').val();
      var machCity = goalForm.find('input[name="city"]').val();
      var machDescr = goalForm.find('input[name="Description"]').val();
      var machRemark = goalForm.find('input[name="remark"]').val();

      console.log('modifyConnect');
      console.log(num);

      if (machName === '' || machName == null) {
        alert('Name can be null!');
      } else {
        let kk = [machName, machCountry, machCity, machDescr, machRemark];
        for (let i = 0; i < kk.length; i += 1) {
          if (kk[i] !== '') {
            switch (i) {
              case 0: Vue.set(obj[num], 'mach_name', machName); break;

              case 1: Vue.set(obj[num], 'mach_country', machCountry); break;

              case 2: Vue.set(obj[num], 'mach_city', machCity); break;

              case 3: Vue.set(obj[num], 'mach_descr', machDescr); break;

              case 4: Vue.set(obj[num], 'mach_remark', machRemark); break;
              default:
            }
          }
        }
        // Vue.set(obj[num], 'mach_name', machName);
        // Vue.set(obj[num], 'mach_country', machCountry);
        // Vue.set(obj[num], 'mach_city', machCity);
        // Vue.set(obj[num], 'mach_descr', machDescr);
        // Vue.set(obj[num], 'mach_remark', machRemark);
        console.log(obj);
        $('#addNewConnet').css('display', 'none');
      }
    },

    show_addDevice: function () {
      $('#addNewDevice .formName span').text('New Device');
      $('#addNewDeviceBtn').css('display', 'block');
      $('#editDeviceBtn').css('display', 'none');
      // $('input#device_name').removeAttr('disabled');
      $('#addNewDevice').css('display', 'block');
    },
    addNewDevice: function () {
      var deviceobj = this.deviceItems;
      var goalForm = $('#addNewDevice');
      var deviceName = goalForm.find('input[name="deviceName"]').val();
      var deviceIp = goalForm.find('input[name="IP"]').val();
      var deviceMask = goalForm.find('input[name="Mask"]').val();
      var deviceDescr = goalForm.find('input[name="Description"]').val();
      var deviceInfo = goalForm.find('input[name="information"]').val();
      var connectType = goalForm.find('input[name="connectType"]').val();
      console.log('addNewDevice');
      console.log(deviceobj);


      if (deviceName === '' || deviceName == null) {
        alert('Name can be null!');
      } else {
        deviceobj.push({
          device_name: deviceName,
          device_ip: deviceIp,
          device_Mask: deviceMask,
          device_descr: deviceDescr,
          device_info: deviceInfo,
          connectType: connectType
        });
        console.log(deviceobj);
        $('#addNewDevice').css('display', 'none');
      }
    },

    show_editDevice: function () {
      $('#addNewDevice .formName span').text('Edit Device');
      $('#addNewDeviceBtn').css('display', 'none');
      $('#editDeviceBtn').css('display', 'block');
      $('#addNewDevice').css('display', 'block');
      // $('input#device_name').attr('disabled','disabled');
    },
    editDevice: function () {
      var deviceobj = this.deviceItems;
      var goalForm = $('#addNewDevice');
      var deviceName = goalForm.find('input[name="deviceName"]').val();
      var deviceIp = goalForm.find('input[name="IP"]').val();
      var deviceMask = goalForm.find('input[name="Mask"]').val();
      var deviceDescr = goalForm.find('input[name="Description"]').val();
      var deviceInfo = goalForm.find('input[name="information"]').val();
      var connectType = goalForm.find('input[name="connectType"]').val();
      console.log('Edit Device');
      console.log(deviceobj);


      deviceobj.push({
        device_name: deviceName,
        device_ip: deviceIp,
        device_Mask: deviceMask,
        device_descr: deviceDescr,
        device_info: deviceInfo,
        connectType: connectType
      });
      console.log(deviceobj);
      $('#addNewDevice').css('display', 'none');
    }
  }
});
